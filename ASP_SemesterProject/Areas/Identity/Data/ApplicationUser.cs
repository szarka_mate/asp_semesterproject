﻿using ASP_SemesterProject.Data.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_SemesterProject.Areas.Identity.Data
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string BillingZIPCode { get; set; }
        public string BillingCountry { get; set; }
        public string BillingCity { get; set; }
        public string BillingAddress { get; set; }
        public string ShippingAddress { get; set; }

        public ICollection<Order> Orders { get; set; }

        public string ShoppingCartId { get; set; }
        [ForeignKey("ShoppingCartId")]
        public ShoppingCart ShoppingCart { get; set; }
    }
}
