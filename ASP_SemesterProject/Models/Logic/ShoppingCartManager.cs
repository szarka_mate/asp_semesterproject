﻿using ASP_SemesterProject.Areas.Identity.Data;
using ASP_SemesterProject.Data;
using ASP_SemesterProject.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_SemesterProject.Models.Logic
{
    // TODO: Test class (not sure if EF automatically builds connections 
    // by adding the appropriate elements to the DB
    public class ShoppingCartManager
    {
        private readonly ApplicationDbContext _context;
        private readonly ItemManager _itemManager;
        public ShoppingCartManager(
            ApplicationDbContext context,
            ItemManager itemManager)
        {
            _context = context;
            _itemManager = itemManager;
        }

        public void AddUserToNewCart(ApplicationUser user)
        {
            _context.Users.Update(user);
            user.ShoppingCart = new ShoppingCart()
            {
                User = user,
                UserId = user.Id
            };
            _context.SaveChanges();
        }

        public ShoppingCart GetCartForUser(ApplicationUser user)
        {
            return _context.ShoppingCarts.Include(sc => sc.ShoppingCartItems)
                .ThenInclude(ci=> ci.Item)
                .Where(sc => sc.UserId == user.Id).FirstOrDefault();
        }

        public ShoppingCart GetCartForUserById(string id)
        {
            return _context.ShoppingCarts.Include(sc => sc.ShoppingCartItems)
                .ThenInclude(ci => ci.Item)
                .Where(sc => sc.UserId == id).FirstOrDefault();
        }

        public void ClearUserShoppingCart(ApplicationUser user)
        {
            GetCartForUser(user).ShoppingCartItems.Clear();
            _context.SaveChanges();
        }

        public void ClearUserIdShoppingCart(string userId)
        {
            GetCartForUserById(userId).ShoppingCartItems.Clear();
            _context.SaveChanges();
        }

        public void DeleteItemFromUserCart(ApplicationUser user, string itemId)
        {
            var cartItemToRemove = _context.ShoppingCartItems.Find(itemId);
            GetCartForUser(user).ShoppingCartItems.Remove(cartItemToRemove);
            _context.SaveChanges();
        }
        public void DeleteItemFromUserCart(string userId, ShoppingCartItem item)
        {

            GetCartForUserById(userId).ShoppingCartItems.Remove(item);
            _context.SaveChanges();
        }

        public void AddItemToUserCart(ApplicationUser user, ShoppingCartItem item)
        {
            GetCartForUser(user).ShoppingCartItems.Add(item);
            _context.SaveChanges();
        }

        public void AddItemToUserCart(string userId, ShoppingCartItem item)
        {
            GetCartForUserById(userId).ShoppingCartItems.Add(item);
            _context.SaveChanges();
        }

        public void AdditemToUserCart(ApplicationUser user, Item item, int count)
        {
            GetCartForUser(user).ShoppingCartItems.Add(new ShoppingCartItem()
            {
                Item = item,
                ItemId = item.Id,
                Count = count
            });
            _context.SaveChanges();
        }

        public void AdditemToUserCart(string userId, Item item, int count)
        {
            GetCartForUserById(userId).ShoppingCartItems.Add(new ShoppingCartItem()
            {
                Item = item,
                ItemId = item.Id,
                Count = count
            });
            _context.SaveChanges();
        }

        public void AdditemToUserCart(ApplicationUser user, string itemId, int count)
        {
            var itemToAdd = _itemManager.GetItemById(itemId);
            GetCartForUser(user).ShoppingCartItems.Add(new ShoppingCartItem()
            {
                Item = itemToAdd,
                ItemId = itemId,
                Count = count
            });
            _context.SaveChanges();
        }

        public void AdditemToUserCart(string userId, string itemId, int count)
        {
            var itemToAdd = _itemManager.GetItemById(itemId);
            GetCartForUserById(userId).ShoppingCartItems.Add(new ShoppingCartItem()
            {
                Item = itemToAdd,
                ItemId = itemId,
                Count = count
            });
            _context.SaveChanges();
        }

        #region Async methods

        public async Task AddUserToNewCartAsync(ApplicationUser user)
        {
            _context.Users.Update(user);
            user.ShoppingCart = new ShoppingCart()
            {
                User = user,
                UserId = user.Id
            };
            await _context.SaveChangesAsync();
        }

        public async Task<ShoppingCart> GetCartForUserAsync(ApplicationUser user)
        {
            return await _context.ShoppingCarts.Include(sc => sc.ShoppingCartItems)
                .ThenInclude(ci => ci.Item)
                .Where(sc => sc.UserId == user.Id).FirstOrDefaultAsync();
        }

        public async Task<ShoppingCart> GetCartForUserByIdAsync(string id)
        {
            return await _context.ShoppingCarts.Include(sc => sc.ShoppingCartItems)
                .ThenInclude(ci => ci.Item)
                .Where(sc => sc.UserId == id).FirstOrDefaultAsync();
        }

        public async Task ClearUserShoppingCartAsync(ApplicationUser user)
        {
            var cart = await GetCartForUserAsync(user);
            cart.ShoppingCartItems.Clear();
            await _context.SaveChangesAsync();
        }

        public async Task ClearUserIdShoppingCartAsync(string userId)
        {
            var cart = await GetCartForUserByIdAsync(userId);
            cart.ShoppingCartItems.Clear();
            await _context.SaveChangesAsync();
        }

        public async Task DeleteItemFromUserCartAsync(ApplicationUser user, string itemId)
        {
            var cartItemToRemove = await _context.ShoppingCartItems.FindAsync(itemId);

            GetCartForUser(user).ShoppingCartItems.Remove(cartItemToRemove);
            await _context.SaveChangesAsync();
        }
        public async Task DeleteItemFromUserCartAsync(string userId, ShoppingCartItem item)
        {
            var cart = await GetCartForUserByIdAsync(userId);

            cart.ShoppingCartItems.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async Task AddItemToUserCartAsync(ApplicationUser user, ShoppingCartItem item)
        {
            var cart = await GetCartForUserAsync(user);

            cart.ShoppingCartItems.Add(item);
            await _context.SaveChangesAsync();
        }

        public async Task AddItemToUserCartAsync(string userId, ShoppingCartItem item)
        {
            var cart = await GetCartForUserByIdAsync(userId);

            cart.ShoppingCartItems.Add(item);
            await _context.SaveChangesAsync();
        }

        public async Task AdditemToUserCartAsync(ApplicationUser user, Item item, int count)
        {
            var cart = await GetCartForUserAsync(user);
            cart.ShoppingCartItems.Add(new ShoppingCartItem()
            {
                Item = item,
                ItemId = item.Id,
                Count = count
            });
            await _context.SaveChangesAsync();
        }

        public async Task AdditemToUserCartAsync(string userId, Item item, int count)
        {
            var cart = await GetCartForUserByIdAsync(userId);
            cart.ShoppingCartItems.Add(new ShoppingCartItem()
            {
                Item = item,
                ItemId = item.Id,
                Count = count
            });
            await _context.SaveChangesAsync();
        }

        public async Task AdditemToUserCartAsync(ApplicationUser user, string itemId, int count)
        {
            var itemToAdd = await _itemManager.GetItemByIdAsync(itemId);
            var cart = await GetCartForUserAsync(user);

            cart.ShoppingCartItems.Add(new ShoppingCartItem()
            {
                Item = itemToAdd,
                ItemId = itemId,
                Count = count
            });
            await _context.SaveChangesAsync();
        }

        public async Task AdditemToUserCartAsync(string userId, string itemId, int count)
        {
            var itemToAdd = await _itemManager.GetItemByIdAsync(itemId);
            var cart = await GetCartForUserByIdAsync(userId);

            cart.ShoppingCartItems.Add(new ShoppingCartItem()
            {
                Item = itemToAdd,
                ItemId = itemId,
                Count = count
            });
            await _context.SaveChangesAsync();
        }

        #endregion
    }
}
