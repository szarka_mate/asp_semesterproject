﻿using ASP_SemesterProject.Areas.Identity.Data;
using ASP_SemesterProject.Data;
using ASP_SemesterProject.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_SemesterProject.Models.Logic
{
    // TODO: Test class (not sure if EF automatically builds connections 
    // by adding the appropriate elements to the DB
    public class OrderManager
    {
        private readonly ApplicationDbContext _context;
        public OrderManager(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Order> GetOrders()
        {
            return _context.Orders.ToList();
        }

        public async Task<List<Order>> GetOrdersAsync()
        {
            return await _context.Orders.ToListAsync();
        }

        public List<Order> GetOrdersWithUser()
        {
            return _context.Orders.Include(o => o.User).ToList();
        }

        public async Task<List<Order>> GetOrdersWithUserAsync()
        {
            return await _context.Orders.Include(o => o.User).ToListAsync();
        }

        public List<Order> GetOrdersWithItems()
        {
            return _context.Orders.Include(o => o.OrderItems).ThenInclude(oi => oi.Item).ToList();
        }

        public async Task<List<Order>> GetOrdersWithItemsAsync()
        {
            return await _context.Orders.Include(o => o.OrderItems).ThenInclude(oi => oi.Item).ToListAsync();
        }

        public List<Order> GetOrdersByUserId(string userId)
        {
            return _context.Orders.Include(o => o.OrderItems).ThenInclude(oi => oi.Item).Where(o => o.UserId == userId).ToList();
        }

        public async Task<List<Order>> GetOrdersByUserIdAsync(string userId)
        {
            return await _context.Orders.Include(o => o.OrderItems).ThenInclude(oi => oi.Item).Where(o => o.UserId == userId).ToListAsync();
        }

        public void AddOrder(Order order)
        {
            _context.Orders.Add(order);
            _context.SaveChanges();
        }

        public async Task AddOrderAsync(Order order)
        {
            _context.Orders.Add(order);
            await _context.SaveChangesAsync();
        }

        public void AddOrderByUser(ApplicationUser user,  ShoppingCart shoppingCart)
        {
            var orderToPlace = new Order()
            {
                IsCompleted = false,
                OrderDate = DateTime.Now,
                User = user,
                UserId = user.Id,
                OrderItems = new List<OrderItem>()
            };

            foreach (var item in shoppingCart.ShoppingCartItems)
            {
                orderToPlace.OrderItems.Add(new OrderItem()
                {
                    Count = item.Count,
                    Item = item.Item,
                    ItemId = item.ItemId,
                    Order = orderToPlace,
                    OrderId = orderToPlace.Id
                });
            }

            _context.Orders.Add(orderToPlace);
            _context.SaveChanges();
        }

        public async Task AddOrderByUserAsync(ApplicationUser user, ShoppingCart shoppingCart)
        {
            var orderToPlace = new Order()
            {
                IsCompleted = false,
                OrderDate = DateTime.Now,
                User = user,
                UserId = user.Id,
                OrderItems = new List<OrderItem>()
            };

            foreach (var item in shoppingCart.ShoppingCartItems)
            {
                orderToPlace.OrderItems.Add(new OrderItem()
                {
                    Count = item.Count,
                    Item = item.Item,
                    ItemId = item.ItemId,
                    Order = orderToPlace,
                    OrderId = orderToPlace.Id
                });
            }

            await _context.Orders.AddAsync(orderToPlace);
            await _context.SaveChangesAsync();
        }

        public void AddOrderByUserId(string userId, ShoppingCart shoppingCart)
        {
            var user = _context.Users.Where(u => u.Id == userId).FirstOrDefault();
            var orderToPlace = new Order()
            {
                IsCompleted = false,
                OrderDate = DateTime.Now,
                User = user,
                UserId = userId,
                OrderItems = new List<OrderItem>()
            };

            foreach (var item in shoppingCart.ShoppingCartItems)
            {
                orderToPlace.OrderItems.Add(new OrderItem()
                {
                    Count = item.Count,
                    Item = item.Item,
                    ItemId = item.ItemId,
                    Order = orderToPlace,
                    OrderId = orderToPlace.Id
                });
            }

            _context.Orders.Add(orderToPlace);
            _context.SaveChanges();
        }

        public async Task AddOrderByUserIdAsync(string userId, ShoppingCart shoppingCart)
        {
            var user = await _context.Users.Where(u => u.Id == userId).FirstOrDefaultAsync();
            var orderToPlace = new Order()
            {
                IsCompleted = false,
                OrderDate = DateTime.Now,
                User = user,
                UserId = userId,
                OrderItems = new List<OrderItem>()
            };

            foreach (var item in shoppingCart.ShoppingCartItems)
            {
                orderToPlace.OrderItems.Add(new OrderItem()
                {
                    Count = item.Count,
                    Item = item.Item,
                    ItemId = item.ItemId,
                    Order = orderToPlace,
                    OrderId = orderToPlace.Id
                });
            }

            await _context.Orders.AddAsync(orderToPlace);
            await _context.SaveChangesAsync();
        }

        public void CompleteOrder(Order order)
        {
            _context.Orders.Update(order);
            order.IsCompleted = true;
            _context.SaveChanges();
        }

        public async Task CompleteOrderAsync(Order order)
        {
            _context.Orders.Update(order);
            order.IsCompleted = true;
            await _context.SaveChangesAsync();
        }

        public void CompleteOrderById(string id)
        {
            Order order = _context.Orders.Find(id);
            _context.Orders.Update(order);
            order.IsCompleted = true;
            _context.SaveChanges();
        }

        public async Task CompleteOrderByIdAsync(string id)
        {
            Order order = await _context.Orders.FindAsync(id);
            _context.Orders.Update(order);
            order.IsCompleted = true;
            await _context.SaveChangesAsync();
        }
    }
}
