﻿using ASP_SemesterProject.Data;
using ASP_SemesterProject.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_SemesterProject.Models.Logic
{
    public class ItemManager
    {
        private readonly ApplicationDbContext _context;

        public ItemManager(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Item> GetItems()
        {
            return _context.Items.ToList();
        }

        public async Task<List<Item>> GetItemsAsync()
        {
            return await _context.Items.ToListAsync();
        }

        public Item GetItemById(string id)
        {
            return _context.Items.Find(id);
        }

        public async Task<Item> GetItemByIdAsync(string id)
        {
            return await _context.Items.FindAsync(id);
        }

        public void AddItem(Item item)
        {
            _context.Items.Add(item);
            _context.SaveChanges();
        }

        public async Task AddItemAsync(Item item)
        {
            _context.Items.Add(item);
            await _context.SaveChangesAsync();
        }

        public void UpdateItem(Item item)
        {
            var toUpdate = _context.Items.Find(item.Id);
            _context.Items.Update(toUpdate);
            toUpdate.Name = item.Name;
            toUpdate.Price = item.Price;
            _context.SaveChanges();
        }

        public async Task UpdateItemAsync(Item item)
        {
            var toUpdate = await _context.Items.FindAsync(item.Id);
            _context.Items.Update(toUpdate);
            toUpdate.Name = item.Name;
            toUpdate.Price = item.Price;
            await _context.SaveChangesAsync();
        }
    }
}
