﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP_SemesterProject.Areas.Identity.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ASP_SemesterProject.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<ApplicationUser> userManager;
        public AdminController(RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
        }


        public async Task<IActionResult> Admin2()
        {
            var actualuser = userManager.FindByEmailAsync(User.Identity.Name).Result;
            await userManager.AddToRoleAsync(actualuser, "Admins");
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Admin()
        {
            if (!roleManager.RoleExistsAsync("Admins").Result)
            {
                IdentityRole adminrole = new IdentityRole()
                {
                    Name = "Admins"
                };
                await roleManager.CreateAsync(adminrole);
            }
           
            
            return RedirectToAction("Admin2");
        }


        public IActionResult Index()
        {
            return RedirectToAction(nameof(HomeController.ItemOrdering), "Home");
        }
    }
}