﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASP_SemesterProject.Models;
using ASP_SemesterProject.Data.Entities;
using ASP_SemesterProject.Data;
using ASP_SemesterProject.Models.Logic;
using Microsoft.AspNetCore.Authorization;

namespace ASP_SemesterProject.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ItemManager itemManager;
        public HomeController(ItemManager itemManager)
        {
            this.itemManager = itemManager;
        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [Authorize(Roles = "Admins")]
        public IActionResult ItemCreate()
        {
            //get
            return View();
        }
        [HttpPost]
        [Authorize(Roles = "Admins")]
        public IActionResult ItemCreate(Item item)
        {
            //post
            itemManager.AddItem(item);

            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Admins")]
        public IActionResult ItemEdit(string Id)
        {
            return View(itemManager.GetItemById(Id));
        }

        [HttpPost]
        [Authorize(Roles = "Admins")]
        public IActionResult ItemEdit(Item item)
        {
            itemManager.UpdateItem(item);
          return RedirectToAction("ItemOrdering");
        }

        [HttpGet]
        public IActionResult ItemOrdering()
        {
            return View(itemManager.GetItems());
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
