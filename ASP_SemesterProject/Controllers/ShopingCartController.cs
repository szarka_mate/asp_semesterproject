﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASP_SemesterProject.Areas.Identity.Data;
using ASP_SemesterProject.Data.Entities;
using ASP_SemesterProject.Models.Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ASP_SemesterProject.Controllers
{
    public class ShopingCartController : Controller
    {
       
        private readonly ShoppingCartManager shoppingCartManager;
        private readonly OrderManager orderManager;
        private readonly UserManager<ApplicationUser> userManager;

        public ShopingCartController(  ShoppingCartManager shoppingCartManager, UserManager<ApplicationUser> userManager, OrderManager orderManager)
        {
          
            this.shoppingCartManager = shoppingCartManager;
            this.userManager = userManager;
            this.orderManager = orderManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ShoppingCartItems()
        {
           var list= shoppingCartManager.GetCartForUser(userManager.FindByEmailAsync(User.Identity.Name).Result);
            return View(list);
        }


        [HttpPost]
        public IActionResult CreateShoppingCartItem(string id, int count)
        {
            shoppingCartManager.AdditemToUserCart(userManager.FindByEmailAsync(User.Identity.Name).Result.Id, id, count);
            return RedirectToAction(nameof(HomeController.ItemOrdering), "Home");
        }

        [HttpPost]
        public IActionResult DeleteCartItem(string ItemId)
        {

            shoppingCartManager.DeleteItemFromUserCart(userManager.FindByEmailAsync(User.Identity.Name).Result, ItemId);
            return RedirectToAction(nameof(ShoppingCartItems));
        }

        [HttpPost]
        public IActionResult PlaceOrder(string cartId)
        {
           var cart= shoppingCartManager.GetCartForUserById(userManager.FindByEmailAsync(User.Identity.Name).Result.Id);
            orderManager.AddOrderByUserId(userManager.FindByEmailAsync(User.Identity.Name).Result.Id,cart);
            shoppingCartManager.ClearUserIdShoppingCart(userManager.FindByEmailAsync(User.Identity.Name).Result.Id);

            return RedirectToAction(nameof(Orders));
        }
        [HttpGet]
        public IActionResult Orders(string cartId)
        {
            var list = orderManager.GetOrdersByUserId(userManager.FindByEmailAsync(User.Identity.Name).Result.Id);
            return View(list);
        }

        [HttpGet]
        [Authorize(Roles = "Admins")]
        public IActionResult AdminOrders(string cartId)
        {
            var list = orderManager.GetOrdersWithItems();
            return View(list);
        }
    }
}