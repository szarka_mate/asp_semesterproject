﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_SemesterProject.Data.Entities
{
    public class ItemViewModel: Item
    {
        public int Count { get; set; }
    }
}
